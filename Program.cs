﻿using System;
using System.Collections.Generic;

namespace AbsoluteValuesSumMinimization
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(absoluteValuesSumMinimization( new int[]{2, 3}));
        }

        static int absoluteValuesSumMinimization(int[] a) {
                return a[(a.Length - 1) / 2];
        }

    }
}
